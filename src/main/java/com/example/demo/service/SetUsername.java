package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.controller.model.UsernameRequest;
import com.example.demo.controller.model.UsernameResponse;

@Service
public class SetUsername implements ISetUsername {

	

	@Override
	public UsernameResponse setUserName(UsernameRequest requestInfo) {
		// TODO Auto-generated method stub
		UsernameResponse response=new UsernameResponse();
		if(requestInfo!=null && requestInfo.getAge()>0) {
			if(requestInfo.getAge()<=30) {
				response.setValidate(true);
			}else {
				response.setValidate(false);
			}
		}
		return response;
	}

}
