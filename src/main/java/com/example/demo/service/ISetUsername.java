package com.example.demo.service;

import com.example.demo.controller.model.UsernameRequest;
import com.example.demo.controller.model.UsernameResponse;

public interface ISetUsername {
	public UsernameResponse setUserName(UsernameRequest requestInfo);
}
