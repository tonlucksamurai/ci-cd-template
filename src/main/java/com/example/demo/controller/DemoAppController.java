package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.controller.model.UsernameRequest;
import com.example.demo.controller.model.UsernameResponse;
import com.example.demo.service.ISetUsername;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/DemoApplication")
public class DemoAppController {

	@Autowired
	private ISetUsername iusername;

	@ApiOperation(value = "Inquiry alert to pay profile history.", response = UsernameResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"), @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"), @ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/getName", method = RequestMethod.POST)
	public ResponseEntity<UsernameResponse> getName(@RequestBody UsernameRequest requestInfo, @RequestHeader(name = "X-Username", required = false) String xUser){
		//[Lhao] Coding
		UsernameResponse result=new UsernameResponse();
		try {
			result=iusername.setUserName(requestInfo);
			
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(result, HttpStatus.EXPECTATION_FAILED);
		}
		
	} // end method
} // end class
