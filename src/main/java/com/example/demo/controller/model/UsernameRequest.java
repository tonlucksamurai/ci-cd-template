package com.example.demo.controller.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class UsernameRequest {
	private int userId;
	private String name;
	private String lastname;
	private int age;
}
