package com.example.demo.controller.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class UsernameResponse {
	private boolean validate;
}
