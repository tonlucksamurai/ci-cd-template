package com.example.demo.controller;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DemoResponse {
	private String name;
	private String lastname;
}
